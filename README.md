
#dubbo-customer
### zookeper 使用相关内容

可参考网上博客内容：  
    http://doc.okbase.net/congcong68/archive/112508.html    
生产者代码：  
    http://git.oschina.net/tHero/dubbo-privder   
使用zookeeper必须的jar文件：   
    dubbo-2.5.3.jar   
    zookeeper-3.3.6.jar      
    zkclient-0.1.jar   
    javassist-3.15.0-GA.jar  
    netty-3.2.5.Final.jar   
这些jar文件可以在“生产者代码：”提供的地址中下载  

项目中没有使用maven项目，所有需要ant来构建jar文件，提供给customer来使用，
    网上参考博客:http://blog.csdn.net/telnetor/article/details/701593
    
### 测试使用流程

1、打开在http://git.oschina.net/tHero/dubbo-customer中的 dubbo-customer\依赖文件 ；    
2、将dubbo-admin-2.5.4.war部署到tomcat webapps下；    
3、导入dubbo-privder和dubbo-customer文件夹下；   
4、将dubbo-customer\依赖文件 中的tomcat server配置内容.txt 粘贴到tomcat config server.xml 并修改其中的修改其中的docBase目录，目录为 dubbo-privder和dubbo-customer两个项目的WebContent目录（绝对路径）   
5、启动本地的zookeeper ，可参考上面的博客地址；   
6、访问dubbo-customer项目中的http://localhost:8061/dubbo-customer/sendMoney/createCustomer
地址，可以在控制台查看输出的内容；   

### 相关代码介绍---生产者    
> zookeeper使用需要有消费者和生产者，生产者提供制定的接口内容后，消费者才可以使用；  


```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	                    http://www.springframework.org/schema/beans/spring-beans.xsd
	                    http://code.alibabatech.com/schema/dubbo
	                    http://code.alibabatech.com/schema/dubbo/dubbo.xsd">
 <!-- 提供方应用名称信息，这个相当于起一个名字，我们dubbo管理页面比较清晰是哪个应用暴露出来的 -->
   <dubbo:application name="dubbo_provider"></dubbo:application>
   <!-- 使用zookeeper注册中心暴露服务地址 -->  
   <dubbo:registry address="zookeeper://127.0.0.1:2181" check="false" subscribe="false" register=""></dubbo:registry>
  <!-- 要暴露的服务接口 -->  
  <dubbo:service interface="com.zooker.RegistryService" ref="testRegistryService" />
</beans>
```   

`dubbo:registry address="zookeeper://127.0.0.1:2181"`表示注册地址；zookeeper在项目启动的时候，将会对连接该地址；   
`dubbo:service` 表示注册的接口内容；其中`ref="testRegistryService"` 是需要在spring配置文件配置的，依获取对象；  
`<bean id="testRegistryService" class="com.zooker.impl.RegistryServiceImpl"></bean>`spring中注入的内容  ；该对象对了的class是个实体类
	
### 消费者    
> 做为消费者，需要知道从何处获取到zookeeper提供的接口；其次，将这些接口引入到自身的服务器中；    

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
 	xmlns:jee="http://www.springframework.org/schema/jee"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
	xmlns:context="http://www.springframework.org/schema/context"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
	http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-3.1.xsd
	http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-3.1.xsd
	http://code.alibabatech.com/schema/dubbo http://code.alibabatech.com/schema/dubbo/dubbo.xsd
	http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.1.xsd"
	default-lazy-init="false" >
   <dubbo:application name="dubbo_consumer"></dubbo:application>
   <!-- 使用zookeeper注册中心暴露服务地址 -->  
   <dubbo:registry address="zookeeper://127.0.0.1:2181" check="false"></dubbo:registry> 
     <!-- 要引用的服务 -->  
   <dubbo:reference interface="com.zooker.RegistryService" id="registryService"></dubbo:reference>
</beans>
```   

`dubbo:registry address="zookeeper://127.0.0.1:2181"`表示生产者获取接口的地址；  
`dubbo:reference interface="com.zooker.RegistryService" id="registryService"`   从zookeeper中获取的接口对象；   
```
	@Autowired   
	RegistryService registryService;
```
在java中只需要将在配置在xml文件中的id对象；即`id="registryService"`添加到对象当中即可 zookeeper 会自动的帮助你找的应该实例化的对象；


